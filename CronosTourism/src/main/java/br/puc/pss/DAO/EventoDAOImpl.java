package br.puc.pss.DAO;

import br.puc.pss.common.Util;
import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Evento;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:20 PM
 */
public class EventoDAOImpl implements EventoDAO {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    DominioDAO dominioDAO = new DominioDAOImpl();
    EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();

    @Override
    public Long insert(Evento obj) throws SQLException {
        String query = "INSERT INTO evento(tipo, datainicio, datafim, pessoaid, alerta, descricao)\n" +
                "    VALUES (?, ?, ?, ?, ?, ?) RETURNING eventoid";
        String resp = run.query(query, new ScalarHandler<Number>(), new Long("1"),
                new java.sql.Date(obj.getDatainicio().getTime()),
                new java.sql.Date(obj.getDatafim().getTime()), obj.getEmpregado().getEmpregadoid(),
                obj.getAlerta(), obj.getDescricao()).toString();
        Long empregadoId = new Long(resp);
        return empregadoId;
    }

    @Override
    public void update(Evento obj) throws SQLException {
        String query = "UPDATE evento SET tipo=?, datainicio=?, datafim=?, pessoaid=?, " +
                "alerta=?, descricao=? WHERE eventoid=?";

        run.update(query, obj.getTipo().getDominioid(),
                new java.sql.Date(obj.getDatainicio().getTime()),
                new java.sql.Date(obj.getDatafim().getTime()), obj.getEmpregado().getEmpregadoid(),
                obj.getAlerta(), obj.getDescricao(), obj.getEventoid());
    }

    @Override
    public ArrayList<Evento> getAll() throws SQLException {
        ArrayList<Evento> lstEventos = new ArrayList<Evento>();
        String query = "SELECT eventoid, tipo, datainicio, datafim, pessoaid, localid, hotlid, \n" +
                "       alerta, descricao FROM evento;\n";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item: resp) {
            lstEventos.add(getObject(item));
        }
        return lstEventos;
    }

    @Override
    public ArrayList<Evento> getAllDateMaior() throws SQLException {
        ArrayList<Evento> lstEventos = new ArrayList<Evento>();
        String query = "SELECT ev.* FROM empregado e, pessoa p, evento ev WHERE p.pessoaid = e.pessoaid " +
                " AND ev.pessoaid = e.empregadoid " +
                " AND ev.datainicio >= current_date AND ev.alerta <> 'EV'";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item: resp) {
            lstEventos.add(getObject(item));
        }
        return lstEventos;
    }

    @Override
    public ArrayList<Evento> getEventosByEmpregadoId(Long id) throws SQLException {
        ArrayList<Evento> lstEventos = new ArrayList<Evento>();
        String query = "SELECT eventoid, tipo, datainicio, datafim, pessoaid, localid, hotlid, \n" +
                "       alerta, descricao FROM evento WHERE pessoaid = "+id;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item: resp) {
            lstEventos.add(getObject(item));
        }
        return lstEventos;
    }


    @Override
    public Evento getById(Long id) throws SQLException {
        String query = "SELECT eventoid, tipo, datainicio, datafim, pessoaid, localid, hotlid, \n" +
                "       alerta, descricao FROM evento WHERE eventoid = "+id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Evento objEvento = null;
        if (resp != null && !resp.isEmpty()) {
            objEvento = getObject(resp);
        }
        return objEvento;
    }

    public Evento getObject(Map<String, Object> item) throws SQLException {
        Evento objEvento = new Evento();
        objEvento.setEventoid(item.get("eventoid") != null ?
                new Long(item.get("eventoid").toString()) : null);
        objEvento.setDatainicio(item.get("datainicio") != null ?
                Util.stringToDate(item.get("datainicio").toString()) : null);
        objEvento.setDatafim(item.get("datafim") != null ?
                Util.stringToDate(item.get("datafim").toString()) : null);
        Long tipoId = item.get("tipo") != null ?
                new Long(item.get("tipo").toString()) : null;
        objEvento.setTipo(dominioDAO.getById(tipoId));
        objEvento.setDescricao((String)item.get("descricao"));
        objEvento.setAlerta((String)item.get("alerta"));
        Long empregadoId = item.get("pessoaid") != null ?
                new Long(item.get("pessoaid").toString()) : null;
        objEvento.setEmpregado(empregadoDAO.getById(empregadoId));
        objEvento.setLocal(null);
        objEvento.setHotel(null);
        return  objEvento;
    }


}
