package br.puc.pss.DAO;

import br.puc.pss.obj.Empregado;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:04 PM
 */
public interface EmpregadoDAO extends BaseDAO<Empregado> {
    ArrayList<Empregado> getByTipoId(Long idTipo) throws SQLException;

    ArrayList<Empregado> getAtivosTipoNasDatas(Long idTipo, Date dataInicio, Date dataFim) throws SQLException;

    ArrayList<Empregado> getAtivosTipoNasDatasTodo(Long idTipo, Date dataInicio, Date dataFim) throws SQLException;
}
