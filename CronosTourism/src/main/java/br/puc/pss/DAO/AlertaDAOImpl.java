package br.puc.pss.DAO;

import br.puc.pss.common.Util;
import br.puc.pss.obj.Alerta;
import br.puc.pss.obj.Evento;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:20 PM
 */
public class AlertaDAOImpl implements AlertaDAO{

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    DominioDAO dominioDAO = new DominioDAOImpl();
    EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    PacoteDAO pacoteDAO = new PacoteDAOImpl();

    @Override
    public Long insert(Alerta obj) throws SQLException {
        String query = "INSERT INTO alerta(tipo, datainicio, datafim, mensajem, estado, pacoteid)\n" +
                "    VALUES (?, ?, ?, ?, ?, ?) RETURNING alertaid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getTipo().getDominioid(),
                obj.getDatainicio() != null ? new java.sql.Date(obj.getDatainicio().getTime()) : null,
                obj.getDatafim() != null ? new java.sql.Date(obj.getDatafim().getTime()) : null,
                obj.getMensajem(), obj.getEstado(), obj.getPacote().getPacoteid()).toString();
        Long empregadoId = new Long(resp);
        return empregadoId;
    }

    @Override
    public void update(Alerta obj) throws SQLException {
        String query = "UPDATE alerta\n" +
                "   SET tipo=?, datainicio=?, datafim=?, mensajem=?, estado=? "+
                " WHERE alertaid=?";

        run.update(query, obj.getTipo().getDominioid(),
                obj.getDatainicio() != null ? new java.sql.Date(obj.getDatainicio().getTime()) : null,
                obj.getDatafim() != null ? new java.sql.Date(obj.getDatafim().getTime()) : null,
                obj.getMensajem(), obj.getEstado(), obj.getAlertaid());
    }

    @Override
    public ArrayList<Alerta> getAll() throws SQLException {
        ArrayList<Alerta> lstAlertas = new ArrayList<Alerta>();
        String query = "SELECT alertaid, tipo, datainicio, datafim, mensajem, estado, pacoteid\n" +
                "  FROM alerta";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item: resp) {
            lstAlertas.add(getObject(item));
        }
        return lstAlertas;
    }

    @Override
    public ArrayList<Alerta> getByEstado(String estado) throws SQLException {
        ArrayList<Alerta> lstAlertas = new ArrayList<Alerta>();
        String query = "SELECT alertaid, tipo, datainicio, datafim, mensajem, estado, pacoteid\n" +
                "  FROM alerta WHERE estado = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), estado);
        for (Map<String, Object> item: resp) {
            lstAlertas.add(getObject(item));
        }
        return lstAlertas;
    }

    @Override
    public Alerta getByTipo(String tipo) throws SQLException {
        ArrayList<Alerta> lstAlertas = new ArrayList<Alerta>();
        String query = "SELECT alertaid, tipo, datainicio, datafim, mensajem, estado, pacoteid\n" +
                "  FROM alerta WHERE tipo = ?";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Alerta objAlerta = null;
        if (resp != null && !resp.isEmpty()) {
            objAlerta = getObject(resp);
        }
        return objAlerta;
    }

    @Override
    public Alerta getByPacote(Long pacoteId) throws SQLException {
        ArrayList<Alerta> lstAlertas = new ArrayList<Alerta>();
        String query = "SELECT alertaid, tipo, datainicio, datafim, mensajem, estado, pacoteid\n" +
                "  FROM alerta WHERE pacoteid = ?";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler(), pacoteId);
        Alerta objAlerta = null;
        if (resp != null && !resp.isEmpty()) {
            objAlerta = getObject(resp);
        }
        return objAlerta;
    }



    @Override
    public Alerta getById(Long id) throws SQLException {
        String query = "SELECT alertaid, tipo, datainicio, datafim, mensajem, estado, pacoteid\n" +
                "  FROM alerta WHERE alertaid = "+id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Alerta objAlerta = null;
        if (resp != null && !resp.isEmpty()) {
            objAlerta = getObject(resp);
        }
        return objAlerta;
    }

    public Alerta getObject(Map<String, Object> item) throws SQLException {
        Alerta objAlerta = new Alerta();
        objAlerta.setAlertaid(item.get("alertaid") != null ?
                new Long(item.get("alertaid").toString()) : null);
        objAlerta.setDatainicio(item.get("datainicio") != null ?
                Util.stringToDate(item.get("datainicio").toString()) : null);
        objAlerta.setDatafim(item.get("datafim") != null ?
                Util.stringToDate(item.get("datafim").toString()) : null);
        Long tipoId = item.get("tipo") != null ?
                new Long(item.get("tipo").toString()) : null;
        objAlerta.setTipo(dominioDAO.getById(tipoId));
        Long pacoteId = item.get("pacoteid") != null ?
                new Long(item.get("pacoteid").toString()) : null;
        objAlerta.setPacote(pacoteDAO.getById(pacoteId));
        objAlerta.setMensajem((String) item.get("mensajem"));
        objAlerta.setEstado((String) item.get("estado"));
        return  objAlerta;
    }


}
