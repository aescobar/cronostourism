package br.puc.pss.DAO;

import br.puc.pss.obj.Pacote;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:06 PM
 */
public interface PacoteDAO extends BaseDAO<Pacote> {

    ArrayList<Pacote> getAllByDateIncioAndFim(Date dataInicio, Date dataFim) throws SQLException;

    ArrayList<Pacote> getPacotesPessoaDatas(Long empregadoid, Date dataInicio, Date dataFim) throws SQLException;
}
