package br.puc.pss.DAO;

import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Usuario;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:19 PM
 */
public class EmpregadoDAOImpl implements EmpregadoDAO {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    PessoaDAO pessoaDAO = new PessoaDAOImpl();
    DominioDAO dominioDAO = new DominioDAOImpl();

    @Override
    public Long insert(Empregado obj) throws SQLException {
        Long pessoaId = pessoaDAO.insert(obj);
        String query = "INSERT INTO empregado(pessoaid, tipo, alertas)\n" +
                "    VALUES (" + pessoaId + ", " + obj.getTipo().getDominioid() + ", ?) RETURNING empregadoid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getAlertas()).toString();
        Long empregadoId = new Long(resp);
        return empregadoId;
    }

    @Override
    public void update(Empregado obj) throws SQLException {
        pessoaDAO.update(obj);
        String query = "UPDATE empregado\n" +
                "        SET tipo= " + obj.getTipo().getDominioid() +
                " , alertas = ? WHERE empregadoid=" + obj.getEmpregadoid();
        run.update(query, obj.getAlertas());
    }

    @Override
    public ArrayList<Empregado> getAll() throws SQLException {
        ArrayList<Empregado> lstEmpregados = new ArrayList<Empregado>();
        String query = "SELECT empregadoid, pessoaid, tipo, alertas\n" +
                "  FROM empregado";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            lstEmpregados.add(getObject(item));
        }
        return lstEmpregados;
    }

    @Override
    public ArrayList<Empregado> getByTipoId(Long idTipo) throws SQLException {
        ArrayList<Empregado> lstEmpregados = new ArrayList<Empregado>();
        String query = "SELECT empregadoid, pessoaid, tipo, alertas\n" +
                "  FROM empregado WHERE tipo = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), idTipo);

        for (Map<String, Object> item : resp) {
            lstEmpregados.add(getObject(item));
        }
        return lstEmpregados;
    }

    @Override
    public ArrayList<Empregado> getAtivosTipoNasDatas(Long idTipo, Date dataInicio, Date dataFim) throws SQLException {
        ArrayList<Empregado> lstEmpregados = new ArrayList<Empregado>();
        String query = "SELECT em.* FROM empregado em, pessoa pes\n" +
                "WHERE em.pessoaid = pes.pessoaid AND em.tipo = ? AND em.ativo = 'S' AND em.empregadoid NOT IN(\n" +
                "SELECT e.empregadoid FROM empregado e, pessoa p, evento ev \n" +
                "WHERE p.pessoaid = e.pessoaid AND ev.pessoaid = e.empregadoid AND ev.datainicio >= current_date\n" +
                "AND ((ev.datainicio <= ? AND ev.datafim >= ?) OR \n" +
                "(ev.datainicio <= ? AND ev.datafim >= ?) OR\n" +
                "(ev.datainicio > ? AND ev.datafim < ?)) AND e.tipo = ?)\n";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), idTipo, new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataInicio.getTime()), new java.sql.Date(dataFim.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataFim.getTime()), idTipo);

        for (Map<String, Object> item : resp) {
            lstEmpregados.add(getObject(item));
        }
        return lstEmpregados;
    }

    @Override
    public ArrayList<Empregado> getAtivosTipoNasDatasTodo(Long idTipo, Date dataInicio, Date dataFim) throws SQLException {
        ArrayList<Empregado> lstEmpregados = new ArrayList<Empregado>();
        String query = "SELECT em.* FROM empregado em WHERE em.empregadoid NOT IN(\n" +
                "                SELECT e.empregadoid FROM empregado e, pessoa p, pacote pa, diapacote dp \n" +
                "                WHERE e.pessoaid = p.pessoaid AND dp.pacoteid = pa.pacoteid AND (dp.guia = e.empregadoid OR dp.prestatarioid = e.empregadoid) \n" +
                "                AND e.tipo = ?\n" +
                "                AND ((pa.datainicio <= ? AND pa.datafim >= ?) OR (pa.datainicio <= ? AND pa.datafim >= ?) \n" +
                "                OR (? <= pa.datainicio  AND ? >= pa.datainicio) OR (? <= pa.datafim  AND ? >= pa.datafim )))\n" +
                "                AND em.empregadoid NOT IN( \n" +
                "                SELECT e.empregadoid FROM empregado e, pessoa p, evento ev \n" +
                "                WHERE p.pessoaid = e.pessoaid AND ev.pessoaid = e.empregadoid AND ev.datainicio >= current_date\n" +
                "                AND ((ev.datainicio <= ? AND ev.datafim >= ?) OR \n" +
                "                (ev.datainicio <= ? AND ev.datafim >= ?) OR\n" +
                "                (ev.datainicio > ? AND ev.datafim < ?)) AND e.tipo = ?) AND em.tipo = ? AND em.alertas = 'S'";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), idTipo, new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataInicio.getTime()), new java.sql.Date(dataFim.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataInicio.getTime()), new java.sql.Date(dataFim.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataFim.getTime()), idTipo, idTipo);

        for (Map<String, Object> item : resp) {
            lstEmpregados.add(getObject(item));
        }
        return lstEmpregados;
    }


    @Override
    public Empregado getById(Long id) throws SQLException {
        String query = "SELECT empregadoid, pessoaid, tipo, alertas\n" +
                "  FROM empregado WHERE empregadoid = " + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Empregado objEmpregado = null;
        if (resp != null && !resp.isEmpty()) {
            objEmpregado = getObject(resp);
        }
        return objEmpregado;
    }

    private Empregado getObject(Map<String, Object> item) throws SQLException {
        Empregado objEmpregado = null;
        Long pessoaId = item.get("pessoaid") != null ?
                new Long(item.get("pessoaid").toString()) : null;
        objEmpregado = new Empregado(pessoaDAO.getById(pessoaId));
        objEmpregado.setEmpregadoid(item.get("empregadoid") != null ?
                new Long(item.get("empregadoid").toString()) : null);
        Long tipoId = item.get("tipo") != null ?
                new Long(item.get("tipo").toString()) : null;
        objEmpregado.setTipo(dominioDAO.getById(tipoId));
        objEmpregado.setAlertas((String)item.get("alertas"));
        return objEmpregado;
    }
}
