package br.puc.pss.DAO;

import br.puc.pss.obj.Decorator.DiaPacote;
import br.puc.pss.obj.DiaPacoteConcrete;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:21 PM
 */
public interface DiaPacoteDAO extends BaseDAO<DiaPacote> {

    ArrayList<DiaPacote> getByPacoteId(Long id) throws SQLException;
}
