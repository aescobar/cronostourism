package br.puc.pss.DAO;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 4:18 AM
 */
public class ConnectDB {
    static final Logger LOGGER = Logger.getLogger(ConnectDB.class);
    private static DataSource dataSource = null;
    private static String host;
    private static String port;
    private static String bd;
    private static String user;
    private static String password;


    public static DataSource getDataSource() {
        Properties prop = new Properties();
        try {
            prop.load(Resources.getResource("config.properties").openStream());
            //new FileInputStream("config.properties")
        } catch (IOException e) {
            LOGGER.error("No se puede obtener el archivo de configuracion de la BD", e);
            return null;
        }

        host = prop.getProperty("bd.host");
        port = prop.getProperty("bd.port");
        bd = prop.getProperty("bd.bd");
        user = prop.getProperty("bd.user");
        password = prop.getProperty("bd.password");

        if (dataSource == null) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                LOGGER.error("Error al obtener el driver de Postgres", e);
            }
            BasicDataSource basicDs = new BasicDataSource();

            basicDs.setUrl("jdbc:postgresql://" + host + ":" + port + "/" + bd);
            basicDs.setUsername(user);
            basicDs.setPassword(password);
            ConnectDB.dataSource = basicDs;
        }
        return dataSource;
    }


}
