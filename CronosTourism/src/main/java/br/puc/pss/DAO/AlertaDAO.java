package br.puc.pss.DAO;

import br.puc.pss.obj.Alerta;
import br.puc.pss.obj.Decorator.DiaPacote;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:21 PM
 */
public interface AlertaDAO extends BaseDAO<Alerta> {


    ArrayList<Alerta> getByEstado(String estado) throws SQLException;

    Alerta getByTipo(String tipo) throws SQLException;

    Alerta getByPacote(Long pacoteId) throws SQLException;
}
