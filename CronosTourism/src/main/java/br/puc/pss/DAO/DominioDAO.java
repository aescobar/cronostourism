package br.puc.pss.DAO;

import br.puc.pss.obj.Dominio;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:21 PM
 */
public interface DominioDAO extends BaseDAO<Dominio> {

    ArrayList<Dominio> getByGrupo(String grupo) throws SQLException;

    Dominio getByGrupoValor(String grupo, String valor) throws SQLException;
}
