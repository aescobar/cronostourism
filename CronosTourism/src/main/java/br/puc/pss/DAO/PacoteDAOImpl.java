package br.puc.pss.DAO;

import br.puc.pss.common.Util;
import br.puc.pss.obj.Evento;
import br.puc.pss.obj.Pacote;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:21 PM
 */
public class PacoteDAOImpl implements PacoteDAO {
    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    DominioDAO dominioDAO = new DominioDAOImpl();
    EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();

    @Override
    public Long insert(Pacote obj) throws SQLException {
        String query = "INSERT INTO pacote(nome, descricao, tipo, estado, dataInicio, dataFim)" +
                "    VALUES (?, ?, ?, ?, ?, ?) RETURNING pacoteid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getNome(), obj.getDescricao(),
                obj.getTipo().getDominioid(), obj.getEstado(), new java.sql.Date(obj.getDataInicio().getTime()),
                new java.sql.Date(obj.getDataFim().getTime())).toString();
        Long pacoteId = new Long(resp);
        return pacoteId;
    }

    @Override
    public void update(Pacote obj) throws SQLException {
        String query = "UPDATE pacote SET nome=?, descricao=?, tipo=?, estado=?, dataInicio=?, \n" +
                "       dataFim=?  WHERE pacoteid=?";

        run.update(query, obj.getNome(), obj.getDescricao(),
                obj.getTipo().getDominioid(), obj.getEstado(), new java.sql.Date(obj.getDataInicio().getTime()),
                new java.sql.Date(obj.getDataFim().getTime()), obj.getPacoteid());
    }


    @Override
    public ArrayList<Pacote> getAll() throws SQLException {
        ArrayList<Pacote> lstPacotes = new ArrayList<Pacote>();
        String query = "SELECT pacoteid, nome, descricao, tipo, estado, dataFim, dataInicio" +
                "  FROM pacote";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item : resp) {
            lstPacotes.add(getObject(item));
        }
        return lstPacotes;
    }

    @Override
    public ArrayList<Pacote> getAllByDateIncioAndFim(Date dataInicio, Date dataFim) throws SQLException {
        ArrayList<Pacote> lstPacotes = new ArrayList<Pacote>();
        List<Map<String, Object>> resp = null;
        if (dataInicio != null && dataFim == null) {
            String query = "SELECT pacoteid, nome, descricao, tipo, estado, dataFim, dataInicio" +
                    "  FROM pacote WHERE datafim >= ?";
            resp = run.query(query, new MapListHandler(), new java.sql.Date(dataInicio.getTime()));
        }
        for (Map<String, Object> item : resp) {
            lstPacotes.add(getObject(item));
        }

        return lstPacotes;
    }

    @Override
    public ArrayList<Pacote> getPacotesPessoaDatas(Long empregadoid, Date dataInicio, Date dataFim) throws SQLException {
        ArrayList<Pacote> lstPacotes = new ArrayList<Pacote>();
        List<Map<String, Object>> resp = null;

        String query = "SELECT pa.* FROM empregado e, pacote pa, diapacote dp \n" +
                "WHERE pa.pacoteid = dp.pacoteid AND (dp.prestatarioid = ? or dp.guia = ?) AND e.empregadoid = ?\n" +
                "AND ((pa.datainicio <= ? AND pa.datafim >= ?) OR (pa.datainicio <= ? AND pa.datafim >= ?))" +
                " AND pa.datainicio > current_date";
        resp = run.query(query, new MapListHandler(), empregadoid, empregadoid, empregadoid,
                new java.sql.Date(dataInicio.getTime()),new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataFim.getTime()), new java.sql.Date(dataFim.getTime()));
        for (Map<String, Object> item : resp) {
            lstPacotes.add(getObject(item));
        }
        return lstPacotes;
    }

    @Override
    public Pacote getById(Long id) throws SQLException {
        String query = "SELECT pacoteid, nome, descricao, tipo, estado, dataFim, dataInicio" +
                " FROM pacote WHERE pacoteid = " + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Pacote objPacote = null;
        if (resp != null && !resp.isEmpty()) {
            objPacote = getObject(resp);
        }
        return objPacote;
    }


    public Pacote getObject(Map<String, Object> item) throws SQLException {
        Pacote objPacote = new Pacote();
        objPacote.setPacoteid(item.get("pacoteid") != null ?
                new Long(item.get("pacoteid").toString()) : null);
        objPacote.setDataInicio(item.get("datainicio") != null ?
                Util.stringToDate(item.get("datainicio").toString()) : null);
        objPacote.setDataFim(item.get("datafim") != null ?
                Util.stringToDate(item.get("datafim").toString()) : null);
        Long tipoId = item.get("tipo") != null ?
                new Long(item.get("tipo").toString()) : null;
        objPacote.setTipo(dominioDAO.getById(tipoId));
        objPacote.setDescricao((String) item.get("descricao"));
        objPacote.setNome((String) item.get("nome"));
        objPacote.setEstado((String) item.get("estado"));
        return objPacote;
    }
}
