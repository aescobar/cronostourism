package br.puc.pss.DAO;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */
public interface BaseDAO<T> {

    Long insert(T obj) throws SQLException;

    void update(T obj) throws SQLException;

    ArrayList<T> getAll() throws SQLException;

    T getById(Long id) throws SQLException;
}
