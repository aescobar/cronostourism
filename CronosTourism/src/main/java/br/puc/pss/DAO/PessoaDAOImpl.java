package br.puc.pss.DAO;

import br.puc.pss.obj.Pessoa;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:21 PM
 */
public class PessoaDAOImpl implements PessoaDAO {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());

    @Override
    public Long insert(Pessoa obj) throws SQLException {
        String query = "INSERT INTO pessoa(nome, sobrenome, docid, telefone, endereco, email)\n" +
                " VALUES (?, ?, ?, ?, ?, ?) RETURNING pessoaid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getNome(), obj.getSobrenome(), obj.getDocid(),
                obj.getTelefone(), obj.getEndereco(), obj.getEmail()).toString();
        Long pessoaId = new Long(resp);
        return pessoaId;
    }

    @Override
    public void update(Pessoa obj) throws SQLException {
        String query = "UPDATE pessoa SET nome=?, sobrenome=?, docid=?, telefone=?, endereco=?, \n" +
                " email=? WHERE pessoaid=" + obj.getPessoaId();
        run.update(query, obj.getNome(), obj.getSobrenome(), obj.getDocid(), obj.getTelefone(), obj.getEndereco(),
                obj.getEmail());
    }

    @Override
    public ArrayList<Pessoa> getAll() {
        return null;
    }

    @Override
    public Pessoa getById(Long id) throws SQLException {
        String query = "SELECT pessoaid, nome, sobrenome, docid, telefone, endereco, email\n" +
                "  FROM pessoa WHERE pessoaid =" + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Pessoa objPessoa = null;
        if (resp != null && !resp.isEmpty()) {
            objPessoa = new Pessoa();
            objPessoa.setPessoaid(resp.get("pessoaid") != null ?
                    new Long(resp.get("pessoaid").toString()) : null);
            objPessoa.setNome((String) resp.get("nome"));
            objPessoa.setSobrenome((String) resp.get("sobrenome"));
            objPessoa.setEmail((String) resp.get("email"));
            objPessoa.setEndereco((String) resp.get("endereco"));
            objPessoa.setTelefone((String) resp.get("telefone"));
            objPessoa.setDocid((String) resp.get("docid"));
        }
        return objPessoa;
    }
}
