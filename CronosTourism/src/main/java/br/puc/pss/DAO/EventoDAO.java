package br.puc.pss.DAO;

import br.puc.pss.obj.Evento;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:04 PM
 */
public interface EventoDAO extends BaseDAO<Evento> {
    ArrayList<Evento> getEventosByEmpregadoId(Long id) throws SQLException;

    ArrayList<Evento> getAllDateMaior() throws SQLException;
}
