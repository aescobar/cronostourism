package br.puc.pss.DAO;

import br.puc.pss.obj.Usuario;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:06 PM
 */
public interface UsuarioDAO extends BaseDAO<Usuario> {
    Usuario autenticarUsuario(String usuario, String password) throws SQLException;
}
