package br.puc.pss.DAO;

import br.puc.pss.obj.Decorator.DiaPacote;
import br.puc.pss.obj.DiaPacoteConcrete;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:10 PM
 */
public class DiaPacoteDAOImpl implements DiaPacoteDAO {
    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    DominioDAO dominioDAO = new DominioDAOImpl();
    EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    PacoteDAO pacoteDAO = new PacoteDAOImpl();
    @Override
    public Long insert(DiaPacote obj) throws SQLException {
        String query = "INSERT INTO diapacote(local, pacoteid, descricao, prestatarioid, hotelid, \n" +
                "            cafe, almorco, jantar, guia, otros, custototal, custobruto)\n" +
                "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING diapacoteid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getLocal(), obj.getPacote().getPacoteid(), obj.getDescricao(),
                obj.getPrestatario().getEmpregadoid(), null, obj.getCafe(), obj.getAlmorco(), obj.getJantar(),
                obj.getGuia().getEmpregadoid(), obj.getOtros(), obj.getCustoTotal(), obj.getCustobruto()).toString();
        Long pacoteId = new Long(resp);
        return pacoteId;
    }

    @Override
    public void update(DiaPacote obj) throws SQLException {
        String query = "UPDATE diapacote\n" +
                " SET local=?, descricao=?, prestatarioid=?, \n" +
                " hotelid=?, cafe=?, almorco=?, jantar=?, guia=?, otros=?, custototal=?, custobruto=?\n" +
                " WHERE diapacoteid=?";

        run.update(query, obj.getLocal(), obj.getDescricao(),
                obj.getPrestatario().getEmpregadoid(), null, obj.getCafe(), obj.getAlmorco(), obj.getJantar(),
                obj.getGuia().getEmpregadoid(), obj.getOtros(), obj.getCustoTotal(), obj.getCustobruto(), obj.getDiapacoteid());
    }


    @Override
    public ArrayList<DiaPacote> getAll() throws SQLException {
        ArrayList<DiaPacote> lstDiasPacotes = new ArrayList<DiaPacote>();
        String query = "SELECT diapacoteid, local, pacoteid, descricao, prestatarioid, hotelid, \n" +
                " cafe, almorco, jantar, guia, otros, custototal, custobruto\n" +
                " FROM diapacote\n";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item : resp) {
            lstDiasPacotes.add(getObject(item));
        }
        return lstDiasPacotes;
    }

    @Override
    public ArrayList<DiaPacote> getByPacoteId(Long id) throws SQLException {
        ArrayList<DiaPacote> lstDiasPacotes = new ArrayList<DiaPacote>();
        String query = "SELECT diapacoteid, local, pacoteid, descricao, prestatarioid, hotelid, \n" +
                " cafe, almorco, jantar, guia, otros, custototal, custobruto\n" +
                " FROM diapacote WHERE pacoteid = "+id;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());
        for (Map<String, Object> item : resp) {
            lstDiasPacotes.add(getObject(item));
        }
        return lstDiasPacotes;
    }

    @Override
    public DiaPacote getById(Long id) throws SQLException {
        String query = "SELECT diapacoteid, local, pacoteid, descricao, prestatarioid, hotelid, \n" +
                " cafe, almorco, jantar, guia, otros, custototal, custobruto\n" +
                " FROM diapacote WHERE diapacoteid =" + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        DiaPacoteConcrete objDiaPacote = null;
        if (resp != null && !resp.isEmpty()) {
            objDiaPacote = getObject(resp);
        }
        return objDiaPacote;
    }


    public DiaPacoteConcrete getObject(Map<String, Object> item) throws SQLException {
        DiaPacoteConcrete objDiasPacote = new DiaPacoteConcrete();
        objDiasPacote.setDiapacoteid(item.get("diapacoteid") != null ?
                new Long(item.get("diapacoteid").toString()) : null);
        Long pacoteId = item.get("pacoteid") != null ?
                new Long(item.get("pacoteid").toString()) : null;
        objDiasPacote.setPacote(pacoteDAO.getById(pacoteId));
        Long prestatarioId = item.get("prestatarioid") != null ?
                new Long(item.get("prestatarioid").toString()) : null;
        objDiasPacote.setPrestatario(empregadoDAO.getById(prestatarioId));
        Long guiaId = item.get("guia") != null ?
                new Long(item.get("guia").toString()) : null;
        objDiasPacote.setGuia(empregadoDAO.getById(guiaId));
        objDiasPacote.setHotel(null);
        objDiasPacote.setAlmorco((String) item.get("almorco"));
        objDiasPacote.setCafe((String) item.get("cafe"));
        objDiasPacote.setJantar((String) item.get("jantar"));
        objDiasPacote.setDescricao((String) item.get("descricao"));
        objDiasPacote.setOtros((String) item.get("otros"));
        objDiasPacote.setLocal((String) item.get("local"));
        objDiasPacote.setCustoTotal(item.get("custototal") != null ?
                new BigDecimal(item.get("custototal").toString()) : null);
        objDiasPacote.setCustobruto(item.get("custobruto") != null ?
                new BigDecimal(item.get("custobruto").toString()) : null);
        return objDiasPacote;
    }
}
