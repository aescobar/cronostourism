package br.puc.pss.DAO;

import br.puc.pss.obj.Hotel;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:05 PM
 */
public interface HotelDAO extends BaseDAO<Hotel> {
}
