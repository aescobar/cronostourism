package br.puc.pss.DAO;

import br.puc.pss.obj.Dominio;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:13 PM
 */
public class DominioDAOImpl implements DominioDAO {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());

    @Override
    public Long insert(Dominio obj) throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void update(Dominio obj) throws SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ArrayList<Dominio> getAll() throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Dominio getById(Long id) throws SQLException {
        String query = "SELECT dominioid, grupodominio, valor, descricao\n" +
                "  FROM dominio WHERE dominioid =" + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Dominio objDominio = null;
        if (resp != null && !resp.isEmpty()) {
            objDominio = getObject(resp);
        }
        return objDominio;
    }

    @Override
    public Dominio getByGrupoValor(String grupo, String valor) throws SQLException {
        String query = "SELECT dominioid, grupodominio, valor, descricao\n" +
                "  FROM dominio WHERE grupodominio = ? AND valor = ?";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler(), grupo, valor);
        Dominio objDominio = null;
        if (resp != null && !resp.isEmpty()) {
            objDominio = getObject(resp);
        }
        return objDominio;
    }

    @Override
    public ArrayList<Dominio> getByGrupo(String grupo) throws SQLException {
        ArrayList<Dominio> lstDominios = new ArrayList<Dominio>();
        String query = "SELECT dominioid, grupodominio, valor, descricao\n" +
                "  FROM dominio WHERE grupodominio = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), grupo);
        for(Map<String, Object> item : resp){
            lstDominios.add(getObject(item));
        }
        return lstDominios;
    }

    private Dominio getObject(Map<String, Object> item){
        Dominio objDominio = new Dominio();
        objDominio.setDominioid(item.get("dominioid") != null ?
                new Long(item.get("dominioid").toString()) : null);
        objDominio.setGrupodominio((String) item.get("grupodominio"));
        objDominio.setValor((String) item.get("valor"));
        objDominio.setDescricao((String) item.get("descricao"));
        return objDominio;
    }
}
