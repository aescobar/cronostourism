package br.puc.pss.DAO;

import br.puc.pss.obj.Pessoa;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:06 PM
 */
public interface PessoaDAO extends BaseDAO<Pessoa> {
}
