package br.puc.pss.DAO;

import br.puc.pss.obj.Pessoa;
import br.puc.pss.obj.Usuario;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 2:21 PM
 */
public class UsuarioDAOImpl implements UsuarioDAO {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    PessoaDAO pessoaDAO = new PessoaDAOImpl();
    DominioDAO dominioDAO = new DominioDAOImpl();

    @Override
    public Long insert(Usuario obj) throws SQLException {
        Long pessoaId = pessoaDAO.insert(obj);
        String query = "INSERT INTO usuario(pessoaid, nomeusuario, senha, ativo, tipo)\n" +
                "    VALUES ("+pessoaId+", ?, ?, ?, ?) RETURNING pessoaid";
        String resp = run.query(query, new ScalarHandler<Number>(), obj.getNomeusuario(),
                obj.getSenha(), obj.getAtivo(), obj.getTipo().getDominioid()).toString();
        Long usuarioId = new Long(resp);
        return usuarioId;
    }

    @Override
    public void update(Usuario obj) throws SQLException {
        pessoaDAO.update(obj);
        String query = "UPDATE usuario SET nomeusuario=?, senha=?, ativo=?, tipo=? " +
                " WHERE usuarioid=" + obj.getPessoaId();
        run.update(query, obj.getNomeusuario(), obj.getSenha(), obj.getAtivo(),
                obj.getTipo().getDominioid());
    }

    @Override
    public ArrayList<Usuario> getAll() throws SQLException {
        ArrayList<Usuario> lstUsuarios = new ArrayList<Usuario>();
        String query = "SELECT usuarioid, pessoaid, nomeusuario, senha, ativo, tipo\n" +
                "  FROM usuario";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item: resp) {
            lstUsuarios.add(getObject(item));
        }
        return lstUsuarios;
    }

    @Override
    public Usuario getById(Long id) throws SQLException {
        String query = "SELECT usuarioid, pessoaid, nomeusuario, senha, ativo, tipo\n" +
                "  FROM usuario WHERE usuarioid" + id;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Usuario objUsuario = null;
        if (resp != null && !resp.isEmpty()) {
            objUsuario = getObject(resp);
        }
        return objUsuario;
    }

    @Override
    public Usuario autenticarUsuario(String usuario, String password) throws SQLException{
        String query = "SELECT usuarioid, pessoaid, nomeusuario, senha, ativo, tipo\n" +
                "  FROM usuario WHERE nomeusuario = ? AND senha = ?";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler(), usuario, password);
        Usuario objUsuario = null;
        if (resp != null && !resp.isEmpty()) {
            objUsuario = getObject(resp);
        }
        return objUsuario;
    }


    private Usuario getObject(Map<String, Object> item) throws SQLException {
        Usuario objUsuario = null;
        Long pessoaId = item.get("pessoaid") != null ?
                new Long(item.get("pessoaid").toString()) : null;
        objUsuario = new Usuario(pessoaDAO.getById(pessoaId));
        objUsuario.setUsuarioid(item.get("usuarioid") != null ?
                new Long(item.get("usuarioid").toString()) : null);
        objUsuario.setNomeusuario((String) item.get("nomeusuario"));
        objUsuario.setSenha((String) item.get("senha"));
        objUsuario.setSenha2((String) item.get("senha"));
        objUsuario.setAtivo((String) item.get("ativo"));
        Long tipoId = item.get("tipo") != null ?
                new Long(item.get("tipo").toString()) : null;
        objUsuario.setTipo(dominioDAO.getById(tipoId));
        return  objUsuario;
    }
}
