package br.puc.pss.agentes.estrategias;

import br.puc.pss.DAO.*;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Evento;
import br.puc.pss.obj.Pacote;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:58 AM
 */

public class AgenteEnviaAlertaPessoalEstrategia implements Estrategia {

    private static final Logger LOGGER = Logger.getLogger(AgenteEnviaAlertaPessoalEstrategia.class);

    private final EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    private final PacoteDAO pacoteDAO = new PacoteDAOImpl();
    private final EventoDAO eventoDAO = new EventoDAOImpl();

    @Override
    public void execute() {
        ArrayList<Evento> lstEventos = new ArrayList<Evento>();
        try {
            lstEventos = eventoDAO.getAllDateMaior();
            for (Evento item : lstEventos) {
                ArrayList<Pacote> lstPacotes = new ArrayList<Pacote>();
                lstPacotes = pacoteDAO.getPacotesPessoaDatas(item.getEmpregado().getEmpregadoid(), item.getDatainicio(), item.getDatafim());
                for (Pacote itemPacote : lstPacotes) {
                    String sms = itemPacote.getNome() + "Problemas com pacotes, precisa-se pessoal ativo Datas"+ Util.dateToStringShortWebApp(itemPacote.getDataInicio())+
                            " - "+ Util.dateToStringShortWebApp(itemPacote.getDataFim());
                    //String sms = "CRONOS TOURISM - PACOTE: " + itemPacote.getNome()+ " Inconsistente ";
                    ArrayList<Empregado> lstEMpregadosAtivos = empregadoDAO.getAtivosTipoNasDatasTodo(item.getEmpregado().getTipo().getDominioid(), item.getDatainicio(), item.getDatafim());
                    for (Empregado itemEmpregado : lstEMpregadosAtivos) {
                        LOGGER.info("Envio SMS Empregado: "+itemEmpregado.getEmpregadoid()+" - "+itemEmpregado.getNome() + " -> "+sms);
                        String resp = EnvioMensaje.enviarSMS(sms, "Fortaleza", itemEmpregado.getTelefone());
                        LOGGER.info("REPSUESTA: " + resp);
                    }
                    item.setAlerta("EV");
                    eventoDAO.update(item);
                }

            }
        } catch (SQLException e) {
            LOGGER.error("Error ao lir os dados de eventos alertas", e);
        }
        LOGGER.info("Enviando SMS Alerta");

    }
}
