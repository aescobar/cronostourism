package br.puc.pss.agentes;

import jade.core.Agent;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:35 AM
 */

public class AgenteEnviaAlerta extends Agent {
	
	protected void setup(){
		
		super.setup();
		
		addBehaviour(new AgenteEnviaAlertaBehavior(this, 10000));
	}

}
