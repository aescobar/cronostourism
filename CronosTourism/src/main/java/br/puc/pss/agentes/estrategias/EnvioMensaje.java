/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.agentes.estrategias;

import br.puc.pss.common.SistemaCommon;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:48 AM
 */

public class EnvioMensaje {

    private static Logger LOGGER = Logger.getLogger(EnvioMensaje.class);
    private static String mwURL = SistemaCommon.SMS_URL;
    private static String mwUser = SistemaCommon.SMS_USER;
    private static String mwPass = SistemaCommon.SMS_PASSWORD;
    private static String mwProvider = SistemaCommon.SMS_PROVIDER;
    private static String charset = "8";


    public static String enviarSMS(String msgText, String originator, String phone) {
        msgText=msgText.replace(" ","+");
        String cadenaEnvio = mwURL + "?username=" + mwUser + "&password=" + mwPass+ "&originator=" + originator + "&phone=55" + phone + "&msgtext=" + msgText+ "&charset=" + charset;
        LOGGER.info("Envio de mensaje a la URL: "+cadenaEnvio);
        String res = "";
        InputStream inputStream = null;
        try {
            URL url = new URL(cadenaEnvio);
            URLConnection conn = url.openConnection();
            inputStream = conn.getInputStream();
            int r;
            StringBuilder sb = new StringBuilder();
            while ((r = inputStream.read()) != -1) {
                sb.append((char) r);
            }
            res = sb.toString();
            LOGGER.info("Respuesta Mensaje: "+res);
        } catch (Exception e) {
            res = e.getMessage() + " - " + e.toString();
            LOGGER.error("Error al intentar enviar mensaje a MoviWeb", e);
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
        }
        return res;
    }
}
