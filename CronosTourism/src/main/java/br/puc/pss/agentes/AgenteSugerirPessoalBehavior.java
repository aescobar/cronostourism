package br.puc.pss.agentes;


import br.puc.pss.agentes.estrategias.AgenteSugestaoPessoalEstrategia;
import br.puc.pss.agentes.estrategias.Contexto;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class AgenteSugerirPessoalBehavior extends TickerBehaviour {

	public AgenteSugerirPessoalBehavior(Agent a, long period) {
		super(a, period);
	}

	@Override
	protected void onTick() {
	        Contexto contexto = new Contexto(new AgenteSugestaoPessoalEstrategia());
	       	contexto.executeEstrategia();	
	}
	
		
	
}


