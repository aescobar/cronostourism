package br.puc.pss.agentes.estrategias;


import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.obj.Alerta;
import br.puc.pss.obj.Dominio;
import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Pacote;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:52 AM
 */

public class AgenteSugestaoPessoalEstrategia implements Estrategia {

    static final Logger LOGGER = Logger.getLogger(AgenteSugestaoPessoalEstrategia.class);

    private final PacoteDAO pacoteDAO = new PacoteDAOImpl();
    private final EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    private final DominioDAO dominioDAO = new DominioDAOImpl();
    private final AlertaDAO alertaDAO = new AlertaDAOImpl();

    @Override
    public void execute() {
        LOGGER.info("Sugestao Pessoal");

        try {
            Dominio objTipoPrestatario = dominioDAO.getByGrupoValor(SistemaCommon.P_TIPOS_EMPREGADO_BD, SistemaCommon.P_VALOR_PRESTATARIO_BD);
            Dominio objTipoGuia = dominioDAO.getByGrupoValor(SistemaCommon.P_TIPOS_EMPREGADO_BD, SistemaCommon.P_VALOR_GUIA_BD);
            ArrayList<Pacote> lstPacotes = pacoteDAO.getAllByDateIncioAndFim(new Date(), null);
            int sum = 0, max = 0;
            String problemas = "";
            for (int i = 0; i < lstPacotes.size(); i++) {
                max = 1;
                sum = 1;
                Pacote p1 = lstPacotes.get(i);
                Pacote p2 = null;
                Date datainicio = p1.getDataInicio();
                Date datafim = p1.getDataFim();
                problemas = "";
                for (int j = i + 1; j < lstPacotes.size(); j++) {
                    p2 = lstPacotes.get(j);
                    Interval interval = new Interval(new DateTime(p1.getDataInicio()), new DateTime(p1.getDataFim()));
                    if (interval.contains(new DateTime(p2.getDataInicio())) || interval.contains(new DateTime(p2.getDataFim()))) {
                        sum++;
                        problemas += ", " + p2.getNome();
                    }
                }
                if (sum > max) {
                    max = sum;
                    if (datafim.compareTo(p2.getDataFim()) < 0)
                        datafim = p2.getDataFim();
                }
                ArrayList<Empregado> lstEmpregadosPrestatarios = empregadoDAO.getAtivosTipoNasDatas(objTipoPrestatario.getDominioid(), datainicio, datafim);
                ArrayList<Empregado> lstEmpregadosGuias = empregadoDAO.getAtivosTipoNasDatas(objTipoGuia.getDominioid(), datainicio, datafim);

                Alerta guiaAlerta = alertaDAO.getByPacote(p1.getPacoteid());
                if (lstEmpregadosGuias.size() < max || lstEmpregadosPrestatarios.size() < max) {
                    LOGGER.info("Alerta Guia Pessoal");

                    if (guiaAlerta == null) {
                        guiaAlerta = new Alerta();
                        guiaAlerta.setEstado("AC");
                        guiaAlerta.setPacote(p1);
                        problemas = lstEmpregadosGuias.size() < max ? problemas + "Precisa guias" : problemas;
                        problemas = lstEmpregadosPrestatarios.size() < max ? problemas + "Precisa prestatarios" : problemas;
                        guiaAlerta.setMensajem("Problemas com os paquetes" + problemas);
                        guiaAlerta.setTipo(objTipoGuia);
                        alertaDAO.insert(guiaAlerta);
                    } else {
                        guiaAlerta.setEstado("AC");
                        guiaAlerta.setMensajem("Problemas com os paquetes" + problemas);
                        alertaDAO.update(guiaAlerta);
                    }
                } else {
                    if (guiaAlerta != null) {
                        guiaAlerta.setEstado("SA");
                        guiaAlerta.setMensajem("Problemas com os paquetes" + problemas);
                        alertaDAO.update(guiaAlerta);
                    }
                }

            }

        } catch (SQLException e) {
            LOGGER.error("erro consulta ", e);
        }

    }
}
