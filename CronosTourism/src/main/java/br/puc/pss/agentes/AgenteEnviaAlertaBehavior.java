package br.puc.pss.agentes;


import br.puc.pss.agentes.estrategias.AgenteEnviaAlertaPessoalEstrategia;
import br.puc.pss.agentes.estrategias.Contexto;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;


/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:28 AM
 */

public class AgenteEnviaAlertaBehavior extends TickerBehaviour{

	public AgenteEnviaAlertaBehavior(Agent a, long period) {
		super(a, period);
	}


	@Override
	protected void onTick() {
		 Contexto contexto = new Contexto(new AgenteEnviaAlertaPessoalEstrategia());
	     contexto.executeEstrategia();	
	}

}
