package br.puc.pss.agentes.estrategias;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:50 AM
 */

public class Contexto {
	
	 private Estrategia estrategia;
	 
	    public Contexto(Estrategia estrategia) {
	        this.estrategia = estrategia;
	    }
	 
	    public void executeEstrategia() {
	        estrategia.execute();
	    }

}
