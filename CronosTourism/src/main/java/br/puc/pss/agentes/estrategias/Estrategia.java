package br.puc.pss.agentes.estrategias;


/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:42 AM
 */

public interface Estrategia {
	void execute();
}
