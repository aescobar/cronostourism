/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */

public class Empregado extends Pessoa{
    private static final long serialVersionUID = 1L;
    private Long empregadoid;
    private List<Evento> eventoList;
    private Pessoa pessoaid;
    private Dominio tipo;
    private String alertas;

    public Empregado() {
        tipo = new Dominio();
    }

    public Empregado(Long empregadoid) {
        this.empregadoid = empregadoid;
    }

    public Empregado(Empregado obj) {
        super(obj);
        this.empregadoid = obj.getEmpregadoid();
        this.tipo = obj.getTipo();
    }

    public Empregado(Pessoa obj){
        super(obj);
    }

    public Long getEmpregadoid() {
        return empregadoid;
    }

    public void setEmpregadoid(Long empregadoid) {
        this.empregadoid = empregadoid;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    public Pessoa getPessoaid() {
        return pessoaid;
    }

    public void setPessoaid(Pessoa pessoaid) {
        this.pessoaid = pessoaid;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empregadoid != null ? empregadoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empregado)) {
            return false;
        }
        Empregado other = (Empregado) object;
        if ((this.empregadoid == null && other.empregadoid != null) || (this.empregadoid != null && !this.empregadoid.equals(other.empregadoid))) {
            return false;
        }
        return true;
    }

    public String getAlertas() {
        return alertas;
    }

    public void setAlertas(String alertas) {
        this.alertas = alertas;
    }

    @Override
    public String toString() {
        return "obj.Empregado[ empregadoid=" + empregadoid + " ]";
    }

    @Override
    public ArrayList<String> validar(){
        ArrayList<String> erros = super.validar();
        if(tipo == null)
            erros.add("Tipo é preciso");
        return erros;
    }
    
}
