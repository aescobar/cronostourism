package br.puc.pss.obj.Decorator;

import br.puc.pss.common.SistemaCommon;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/21/13
 * Time: 6:37 PM
 */
public class DiaPacoteCafe extends  DiaPacoteDecorator {

    public  DiaPacote diaPacote;
    private  BigDecimal custoCafe;

    public DiaPacoteCafe(DiaPacote diaPacote) {
        this.diaPacote = diaPacote;
    }

    public BigDecimal getCustoCafe() {
        return custoCafe;
    }

    public void setCustoCafe(BigDecimal custoCafe) {
        this.custoCafe = custoCafe;
    }

    @Override
    public String getDescricao() {
        return diaPacote.getDescricao() + " cafe da manha";
    }

    @Override
    public BigDecimal getCustoTotal() {
        return diaPacote.getCustoTotal().add(SistemaCommon.custoCafe);
    }



}
