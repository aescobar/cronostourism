package br.puc.pss.obj.Decorator;

import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Hotel;
import br.puc.pss.obj.Pacote;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/21/13
 * Time: 6:33 PM
 */
public abstract class DiaPacoteDecorator extends DiaPacote {

    public abstract String getDescricao();

    public abstract BigDecimal getCustoTotal();

}
