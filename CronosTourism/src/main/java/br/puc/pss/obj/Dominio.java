/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */
public class Dominio {
    private static final long serialVersionUID = 1L;
    private Long dominioid;
    private String grupodominio;
    private String valor;
    private String descricao;
    private List<Pacote> pacoteList;
    private List<Usuario> usuarioList;
    private Hotel hotel;
    private List<Empregado> empregadoList;

    public Dominio() {
    }

    public Dominio(Long dominioid) {
        this.dominioid = dominioid;
    }

    public Dominio(Long dominioid, String grupodominio, String valor) {
        this.dominioid = dominioid;
        this.grupodominio = grupodominio;
        this.valor = valor;
    }

    public Long getDominioid() {
        return dominioid;
    }

    public void setDominioid(Long dominioid) {
        this.dominioid = dominioid;
    }

    public void setDominioid(String dominioid) {
        this.dominioid = new Long(dominioid);
    }

    public String getGrupodominio() {
        return grupodominio;
    }

    public void setGrupodominio(String grupodominio) {
        this.grupodominio = grupodominio;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Pacote> getPacoteList() {
        return pacoteList;
    }

    public void setPacoteList(List<Pacote> pacoteList) {
        this.pacoteList = pacoteList;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Empregado> getEmpregadoList() {
        return empregadoList;
    }

    public void setEmpregadoList(List<Empregado> empregadoList) {
        this.empregadoList = empregadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dominioid != null ? dominioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dominio)) {
            return false;
        }
        Dominio other = (Dominio) object;
        if ((this.dominioid == null && other.dominioid != null) || (this.dominioid != null && !this.dominioid.equals(other.dominioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Dominio[ dominioid=" + dominioid + " ]";
    }

}
