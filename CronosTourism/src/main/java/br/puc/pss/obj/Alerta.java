package br.puc.pss.obj;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/26/13
 * Time: 2:46 PM
 */
public class Alerta {
    private Long alertaid;
    private Dominio tipo;
    private Date datainicio;
    private Date datafim;
    private String mensajem;
    private String estado;
    private Pacote pacote;

    public Long getAlertaid() {
        return alertaid;
    }

    public void setAlertaid(Long alertaid) {
        this.alertaid = alertaid;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    public Date getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(Date datainicio) {
        this.datainicio = datainicio;
    }

    public Date getDatafim() {
        return datafim;
    }

    public void setDatafim(Date datafim) {
        this.datafim = datafim;
    }

    public String getMensajem() {
        return mensajem;
    }

    public void setMensajem(String mensajem) {
        this.mensajem = mensajem;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }
}
