/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */

public class Evento implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long eventoid;
    private Dominio tipo;
    private Date datainicio;
    private Date datafim;
    private String alerta;
    private String descricao;
    private Local local;
    private Hotel hotel;
    private Empregado empregado;

    public Evento() {
    }

    public Evento(Long eventoid) {
        this.eventoid = eventoid;
    }

    public Evento(Long eventoid, Dominio tipo) {
        this.eventoid = eventoid;
        this.tipo = tipo;
    }

    public Long getEventoid() {
        return eventoid;
    }

    public void setEventoid(Long eventoid) {
        this.eventoid = eventoid;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    public Date getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(Date datainicio) {
        this.datainicio = datainicio;
    }

    public Date getDatafim() {
        return datafim;
    }

    public void setDatafim(Date datafim) {
        this.datafim = datafim;
    }

    public String getAlerta() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta = alerta;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Empregado getEmpregado() {
        return empregado;
    }

    public void setEmpregado(Empregado empregado) {
        this.empregado = empregado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventoid != null ? eventoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.eventoid == null && other.eventoid != null) || (this.eventoid != null && !this.eventoid.equals(other.eventoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Evento[ eventoid=" + eventoid + " ]";
    }

    public ArrayList<String> validar() {
        ArrayList<String> erros = new ArrayList<String>();
        return erros;
    }
}
