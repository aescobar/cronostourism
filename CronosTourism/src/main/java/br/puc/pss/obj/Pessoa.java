/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import br.puc.pss.common.SistemaCommon;
import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ariel
 */

public class Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long pessoaid;
    private String nome;
    private String sobrenome;
    private String docid;
    private String telefone;
    private String endereco;
    private String email;

    public Pessoa() {
    }

    public Pessoa(Long pessoaid) {
        this.pessoaid = pessoaid;
    }

    public Pessoa(Pessoa obj){
        this.pessoaid = obj.getPessoaId();
        this.nome = obj.getNome();
        this.sobrenome = obj.getSobrenome();
        this.docid = obj.getDocid();
        this.telefone = obj.getTelefone();
        this.endereco = obj.getEndereco();
        this.email = obj.getEmail();
    }

    public Long getPessoaId() {
        return pessoaid;
    }

    public void setPessoaid(Long pessoaid) {
        this.pessoaid = pessoaid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pessoaid != null ? pessoaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.pessoaid == null && other.pessoaid != null) || (this.pessoaid != null && !this.pessoaid.equals(other.pessoaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Pessoa[ pessoaid=" + pessoaid + " ]";
    }

    public ArrayList<String> validar(){
        ArrayList<String> erros = new ArrayList<String>();
        Pattern pattern = Pattern.compile(SistemaCommon.P_PADRAO_VALID_EMAIL);
        Matcher matcher = pattern.matcher(email);
        if(!matcher.matches())
            erros.add("Normato de e-mail inválido");
        if(Strings.isNullOrEmpty(nome))
            erros.add("Nome é preciso");
        if(Strings.isNullOrEmpty(telefone))
            erros.add("Telefone é preciso");
        return erros;
    }
    
}
