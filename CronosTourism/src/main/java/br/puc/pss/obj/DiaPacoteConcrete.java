/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;


import br.puc.pss.obj.Decorator.DiaPacote;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */

public class DiaPacoteConcrete extends DiaPacote {
    public DiaPacoteConcrete() {
    }

    public DiaPacoteConcrete(Long diapacoteid) {
        this.diapacoteid = diapacoteid;
    }

    public DiaPacoteConcrete(DiaPacote diaPacote) {
        custobruto = diaPacote.getCustobruto();
        descricao = diaPacote.getDescricao();
        custoTotal = diaPacote.getCustoTotal();
        diapacoteid = diaPacote.getDiapacoteid();
        local = diaPacote.getLocal();
        cafe = diaPacote.getCafe();
        almorco = diaPacote.getAlmorco();
        jantar = diaPacote.getJantar();
        otros = diaPacote.getOtros();
        pacote = diaPacote.getPacote();
        hotel = diaPacote.getHotel();
        guia = diaPacote.getGuia();
        prestatario = diaPacote.getPrestatario();

    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diapacoteid != null ? diapacoteid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiaPacoteConcrete)) {
            return false;
        }
        DiaPacoteConcrete other = (DiaPacoteConcrete) object;
        if ((this.diapacoteid == null && other.diapacoteid != null) || (this.diapacoteid != null && !this.diapacoteid.equals(other.diapacoteid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.DiaPacoteConcrete[ diapacoteid=" + diapacoteid + " ]";
    }


}
