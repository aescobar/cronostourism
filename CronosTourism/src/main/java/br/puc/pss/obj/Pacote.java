/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */

public class Pacote implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long pacoteid;
    private String nome;
    private String descricao;
    private String estado;
    private Date dataFim;
    private Date dataInicio;
    private Dominio tipo;

    public Pacote() {
        tipo = new Dominio();
    }

    public Pacote(Long pacoteid) {
        this.pacoteid = pacoteid;
    }

    public Pacote(Long pacoteid, String nome, String estado) {
        this.pacoteid = pacoteid;
        this.nome = nome;
        this.estado = estado;
    }

    public Long getPacoteid() {
        return pacoteid;
    }

    public void setPacoteid(Long pacoteid) {
        this.pacoteid = pacoteid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pacoteid != null ? pacoteid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacote)) {
            return false;
        }
        Pacote other = (Pacote) object;
        if ((this.pacoteid == null && other.pacoteid != null) || (this.pacoteid != null && !this.pacoteid.equals(other.pacoteid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Pacote[ pacoteid=" + pacoteid + " ]";
    }

    public ArrayList<String> validar() {
        ArrayList<String> erros = new ArrayList<String>();
        return erros;
    }
}
