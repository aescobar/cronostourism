/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import com.google.common.base.Strings;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */
public class Usuario extends Pessoa{
    private static final long serialVersionUID = 1L;
    private Long usuarioid;
    private String nomeusuario;
    private String senha;
    private String senha2;
    private String ativo;
    private Dominio tipo;

    public Usuario() {
        tipo = new Dominio();
    }

    public Usuario(Usuario obj) {
        super(obj);
        this.usuarioid = obj.getUsuarioid();
        this.nomeusuario = obj.getNomeusuario();
        this.senha = obj.getSenha();
        this.ativo = obj.getAtivo();
        this.tipo = obj.getTipo();
    }

    public Usuario(Long usuarioid) {
        this.usuarioid = usuarioid;
    }

    public Usuario(Pessoa obj){
        super(obj);
    }

    public Long getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(Long usuarioid) {
        this.usuarioid = usuarioid;
    }

    public String getNomeusuario() {
        return nomeusuario;
    }

    public void setNomeusuario(String nomeusuario) {
        this.nomeusuario = nomeusuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenha2() {
        return senha2;
    }

    public void setSenha2(String senha2) {
        this.senha2 = senha2;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioid != null ? usuarioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioid == null && other.usuarioid != null) || (this.usuarioid != null && !this.usuarioid.equals(other.usuarioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Usuario[ usuarioid=" + usuarioid + " ]";
    }

    @Override
    public ArrayList<String> validar(){
        ArrayList<String> erros = super.validar();
        if(tipo == null)
            erros.add("Tipo é preciso");
        if(Strings.isNullOrEmpty(nomeusuario))
            erros.add("Nome usuario é preciso");
        if(Strings.isNullOrEmpty(senha))
            erros.add("Senha é precisa");
        else if(Strings.isNullOrEmpty(senha2))
            erros.add("É preciso repetir a Senha");
        else if(!senha.equalsIgnoreCase(senha2))
            erros.add("A senha precisa ser igual");
        return erros;
    }

}
