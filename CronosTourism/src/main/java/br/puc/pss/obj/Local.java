/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.pss.obj;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 1:06 PM
 */


public class Local implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long localid;
    private String nome;
    private String descricao;
    private List<Evento> eventoList;

    public Local() {
    }

    public Local(Long localid) {
        this.localid = localid;
    }

    public Local(Long localid, String nome) {
        this.localid = localid;
        this.nome = nome;
    }

    public Long getLocalid() {
        return localid;
    }

    public void setLocalid(Long localid) {
        this.localid = localid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localid != null ? localid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local)) {
            return false;
        }
        Local other = (Local) object;
        if ((this.localid == null && other.localid != null) || (this.localid != null && !this.localid.equals(other.localid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Local[ localid=" + localid + " ]";
    }

}
