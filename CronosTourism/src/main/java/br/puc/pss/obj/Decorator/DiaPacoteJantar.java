package br.puc.pss.obj.Decorator;

import br.puc.pss.common.SistemaCommon;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/21/13
 * Time: 6:37 PM
 */
public class
        DiaPacoteJantar extends  DiaPacoteDecorator {

    public  DiaPacote diaPacote;

    public DiaPacoteJantar(DiaPacote diaPacote) {
        this.diaPacote = diaPacote;
    }



    @Override
    public String getDescricao() {
        return diaPacote.getDescricao() + " cafe da manha";
    }

    @Override
    public BigDecimal getCustoTotal() {
        return diaPacote.getCustoTotal().add(SistemaCommon.custoCafe);
    }


}
