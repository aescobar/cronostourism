package br.puc.pss.obj.Decorator;

import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Hotel;
import br.puc.pss.obj.Pacote;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/21/13
 * Time: 6:19 PM
 */
public abstract class DiaPacote {
    protected String descricao;
    protected BigDecimal custoTotal;
    protected Long diapacoteid;
    protected String local;
    protected String cafe;
    protected String almorco;
    protected String jantar;
    protected String otros;
    protected Pacote pacote;
    protected Hotel hotel;
    protected Empregado guia;
    protected Empregado prestatario;
    protected BigDecimal custobruto = BigDecimal.ZERO;

    public void setCustoTotal(BigDecimal custoTotal) {
        this.custoTotal = custoTotal;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getDiapacoteid() {
        return diapacoteid;
    }

    public void setDiapacoteid(Long diapacoteid) {
        this.diapacoteid = diapacoteid;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }


    public String getCafe() {
        return cafe;
    }

    public void setCafe(String cafe) {
        this.cafe = cafe;
    }

    public String getAlmorco() {
        return almorco;
    }

    public void setAlmorco(String almorco) {
        this.almorco = almorco;
    }

    public String getJantar() {
        return jantar;
    }

    public void setJantar(String jantar) {
        this.jantar = jantar;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }


    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Empregado getGuia() {
        return guia;
    }

    public void setGuia(Empregado guia) {
        this.guia = guia;
    }

    public Empregado getPrestatario() {
        return prestatario;
    }

    public void setPrestatario(Empregado prestatario) {
        this.prestatario = prestatario;
    }

    public BigDecimal getCustobruto() {
        if(custobruto == null)
            custobruto = BigDecimal.ZERO;
        return custobruto;
    }

    public void setCustobruto(BigDecimal custobruto) {
        this.custobruto = custobruto;
    }


    public String getDescricao() {
        return descricao;
    }


    public BigDecimal getCustoTotal() {
        return custoTotal;
    }

    public ArrayList<String> validar() {
        ArrayList<String> erros = new ArrayList<String>();
        return erros;
    }
}
