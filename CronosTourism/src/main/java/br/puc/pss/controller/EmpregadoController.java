package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Empregado;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "EmpregadoController")
@ViewScoped
public class EmpregadoController {
    static final Logger LOGGER = Logger.getLogger(EmpregadoController.class);
    private EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();

    private Empregado objEmpregadoSelect;
    private ArrayList<Empregado> lstEmpregados;
    private ArrayList<Empregado> lstEmpregadosFilter;

    public EmpregadoController() {
        if (objEmpregadoSelect == null)
            objEmpregadoSelect = new Empregado();
    }

    public Empregado getObjEmpregadoSelect() {
        return objEmpregadoSelect;
    }

    public void setObjEmpregadoSelect(Empregado objEmpregadoSelect) {
        this.objEmpregadoSelect = objEmpregadoSelect;
    }

    public ArrayList<Empregado> getLstEmpregados() {
        try {
            lstEmpregados = empregadoDAO.getAll();
        } catch (SQLException e) {
            LOGGER.error("Nao se pode recuperar os dados de empregados", e);
            lstEmpregados = new ArrayList<Empregado>();
        }
        return lstEmpregados;
    }

    public void setLstEmpregados(ArrayList<Empregado> lstEmpregados) {
        this.lstEmpregados = lstEmpregados;
    }

    public ArrayList<Empregado> getLstEmpregadosFilter() {
        return lstEmpregadosFilter;
    }

    public void setLstEmpregadosFilter(ArrayList<Empregado> lstEmpregadosFilter) {
        this.lstEmpregadosFilter = lstEmpregadosFilter;
    }

    public void atulizarEmpregado(Empregado objEmpregado) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(SistemaCommon.S_EMPREGADO_SELECIONADO, objEmpregado);
            Util.redirecionar("/adm/empregado_edit.jsf");
    }

}
