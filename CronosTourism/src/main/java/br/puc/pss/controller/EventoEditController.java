package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Dominio;
import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Evento;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "EventoEditController")
@ViewScoped
public class EventoEditController {

    static final Logger LOGGER = Logger.getLogger(EventoEditController.class);

    private EventoDAO eventoDAO = new EventoDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();
    private EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();

    private SistemaCommon.ActionSistema estado = SistemaCommon.ActionSistema.novo;

    private Evento objEventoSelect;

    private ArrayList<Empregado> lstEmpregados;
    private ArrayList<Empregado> lstEmpregadosFilter;

    private ArrayList<SelectItem> lstTipo;
    private String titulo;
    private boolean vMensaje = false;
    private boolean vDlgEmpregados = false;

    public EventoEditController() {
        vMensaje = false;
        vDlgEmpregados = false;
        estado = SistemaCommon.ActionSistema.novo;
        titulo = "Novo Evento";
        objEventoSelect = new Evento();
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            objEventoSelect = (Evento) session.getAttribute(SistemaCommon.S_EVENTO_SELECIONADO);
            if (objEventoSelect != null) {
                estado = SistemaCommon.ActionSistema.atualizar;
                titulo = "Editar Evento";
            }
        } catch (Exception e) {
            LOGGER.error("Error lir evento no Edit");
        }
    }

    public Evento getObjEventoSelect() {
        if (objEventoSelect == null)
            objEventoSelect = new Evento();
        return objEventoSelect;
    }

    public void setObjEventoSelect(Evento objEventoSelect) {
        this.objEventoSelect = objEventoSelect;
    }

    public ArrayList<SelectItem> getLstTipo() {
        if (lstTipo == null || lstTipo.size() < 1) {
            try {
                lstTipo = new ArrayList<SelectItem>();
                ArrayList<Dominio> lstPaisesDom = dominioDAO.getByGrupo(SistemaCommon.P_TIPOS_EMPREGADO_BD);
                for (Dominio itemDominio : lstPaisesDom) {
                    SelectItem i = new SelectItem(itemDominio.getDominioid(), itemDominio.getDescricao());
                    lstTipo.add(i);
                }
            } catch (SQLException e) {
                lstTipo = new ArrayList<SelectItem>();
            }
        }
        return lstTipo;
    }

    public void setLstTipo(ArrayList<SelectItem> lstTipo) {
        this.lstTipo = lstTipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public boolean isvDlgEmpregados() {
        return vDlgEmpregados;
    }

    public void setvDlgEmpregados(boolean vDlgEmpregados) {
        this.vDlgEmpregados = vDlgEmpregados;
    }

    public ArrayList<Empregado> getLstEmpregados() {
        return lstEmpregados;
    }

    public void setLstEmpregados(ArrayList<Empregado> lstEmpregados) {
        this.lstEmpregados = lstEmpregados;
    }

    public ArrayList<Empregado> getLstEmpregadosFilter() {
        return lstEmpregadosFilter;
    }

    public void setLstEmpregadosFilter(ArrayList<Empregado> lstEmpregadosFilter) {
        this.lstEmpregadosFilter = lstEmpregadosFilter;
    }

    public void salvarDados() {
        try {
            ArrayList<String> erros = objEventoSelect.validar();
            if (erros.size() > 0)
                Util.addErrosMensajens(erros);
            else {
                if (estado == SistemaCommon.ActionSistema.novo) {
                    objEventoSelect.setAlerta("PE");
                    eventoDAO.insert(objEventoSelect);
                } else
                    eventoDAO.update(objEventoSelect);
                Util.addInfoMessage("Cadastro Feito");
                vMensaje = true;
            }
        } catch (SQLException e) {
            vMensaje = false;
            Util.addErrorMessage("Erro Cadastro");
            LOGGER.error("Error salavar dados empregado", e);
        }
    }

    public void volver() {
        vMensaje = false;
        Util.redirecionar("/adm/evento.jsf");
    }

    public void mostrarEmpregados() {
        try {
            lstEmpregados = empregadoDAO.getAll();
            vDlgEmpregados = true;
        } catch (SQLException e) {
            LOGGER.error("Error listar empregados", e);
        }
    }

    public void selecionarEmpregado(Empregado objEmpregado) {

        objEventoSelect.setEmpregado(objEmpregado);
        vDlgEmpregados = false;
    }

    public void fecharEmpregados() {
        vDlgEmpregados = false;
    }


}
