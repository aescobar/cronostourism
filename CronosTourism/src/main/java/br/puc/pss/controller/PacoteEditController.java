package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Decorator.DiaPacote;
import br.puc.pss.obj.DiaPacoteConcrete;
import br.puc.pss.obj.Dominio;
import br.puc.pss.obj.Pacote;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "PacoteEditController")
@ViewScoped
public class PacoteEditController {

    static final Logger LOGGER = Logger.getLogger(PacoteEditController.class);

    private PacoteDAO pacoteDAO = new PacoteDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();
    private DiaPacoteDAO diaPacoteDAO = new DiaPacoteDAOImpl();

    private SistemaCommon.ActionSistema estado = SistemaCommon.ActionSistema.novo;

    private Pacote objPacoteSelect;

    private ArrayList<DiaPacote> lstDiasPacote;
    private DiaPacote objDiaPacoteSelect;

    private ArrayList<SelectItem> lstTipo;
    private String titulo;
    private boolean vMensaje = false;
    private boolean vDlgDiaPacote = false;

    public PacoteEditController() {
        vMensaje = false;
        estado = SistemaCommon.ActionSistema.novo;
        titulo = "Novo Pacote";
        vDlgDiaPacote = false;
        objPacoteSelect = new Pacote();
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            objPacoteSelect = (Pacote) session.getAttribute(SistemaCommon.S_PACOTE_SELECIONADO);
            if (objPacoteSelect != null) {
                lstDiasPacote = diaPacoteDAO.getByPacoteId(objPacoteSelect.getPacoteid());
                estado = SistemaCommon.ActionSistema.atualizar;
                titulo = "Editar Pacote";
                vDlgDiaPacote = true;
            }
        } catch (Exception e) {
            LOGGER.error("Error lir usuario no Edit", e);
        }
    }

    public Pacote getObjPacoteSelect() {
        if (objPacoteSelect == null)
            objPacoteSelect = new Pacote();
        return objPacoteSelect;
    }

    public void setObjPacoteSelect(Pacote objPacoteSelect) {
        this.objPacoteSelect = objPacoteSelect;
    }

    public ArrayList<SelectItem> getLstTipo() {
        if (lstTipo == null || lstTipo.size() < 1) {
            try {
                lstTipo = new ArrayList<SelectItem>();
                ArrayList<Dominio> lstPaisesDom = dominioDAO.getByGrupo(SistemaCommon.P_TIPOS_PACOTE_BD);
                for (Dominio itemDominio : lstPaisesDom) {
                    SelectItem i = new SelectItem(itemDominio.getDominioid(), itemDominio.getDescricao());
                    lstTipo.add(i);
                }
            } catch (SQLException e) {
                lstTipo = new ArrayList<SelectItem>();
            }
        }
        return lstTipo;
    }

    public void setLstTipo(ArrayList<SelectItem> lstTipo) {
        this.lstTipo = lstTipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public ArrayList<DiaPacote> getLstDiasPacote() {
        return lstDiasPacote;
    }

    public void setLstDiasPacote(ArrayList<DiaPacote> lstDiasPacote) {
        this.lstDiasPacote = lstDiasPacote;
    }

    public DiaPacote getObjDiaPacoteSelect() {
        return objDiaPacoteSelect;
    }

    public void setObjDiaPacoteSelect(DiaPacote objDiaPacoteSelect) {
        this.objDiaPacoteSelect = objDiaPacoteSelect;
    }

    public boolean isvDlgDiaPacote() {
        return vDlgDiaPacote;
    }

    public void setvDlgDiaPacote(boolean vDlgDiaPacote) {
        this.vDlgDiaPacote = vDlgDiaPacote;
    }

    public void salvarDados() {
        try {
            ArrayList<String> erros = objPacoteSelect.validar();
            if (erros.size() > 0)
                Util.addErrosMensajens(erros);
            else {
                if (estado == SistemaCommon.ActionSistema.novo) {
                    objPacoteSelect.setEstado("PE");
                    Long idPacote = pacoteDAO.insert(objPacoteSelect);
                    objPacoteSelect.setPacoteid(idPacote);
                    HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    session.setAttribute(SistemaCommon.S_PACOTE_SELECIONADO, objPacoteSelect);
                } else
                    pacoteDAO.update(objPacoteSelect);
                Util.addInfoMessage("Cadastro Feito");
                vMensaje = true;
            }
        } catch (SQLException e) {
            vMensaje = false;
            Util.addErrorMessage("Erro Cadastro");
            LOGGER.error("Error salavar dados empregado", e);
        }
    }

    public void volver() {
        vMensaje = false;
        Util.redirecionar("/adm/pacote.jsf");
    }

    public void fecharMensaje(){
        vMensaje = false;
        vDlgDiaPacote = true;
    }

    public void atualizarDiaPacote(DiaPacoteConcrete objDiaPacote) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute(SistemaCommon.S_DIA_PACOTE_SELECIONADO, objDiaPacote);
        Util.redirecionar("/adm/dia_pacote_edit.jsf");

    }

}
