package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Decorator.DiaPacote;
import br.puc.pss.obj.Decorator.DiaPacoteAlmorco;
import br.puc.pss.obj.Decorator.DiaPacoteCafe;
import br.puc.pss.obj.Decorator.DiaPacoteJantar;
import br.puc.pss.obj.DiaPacoteConcrete;
import br.puc.pss.obj.Dominio;
import br.puc.pss.obj.Empregado;
import br.puc.pss.obj.Pacote;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "DiaPacoteEditController")
@ViewScoped
public class DiaPacoteEditController {

    static final Logger LOGGER = Logger.getLogger(DiaPacoteEditController.class);

    //    private PacoteDAO pacoteDAO = new PacoteDAOImpl();
    private DiaPacoteDAO diaPacoteDAO = new DiaPacoteDAOImpl();
    private EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();

    private SistemaCommon.ActionSistema estado = SistemaCommon.ActionSistema.novo;
    private Dominio tipoEmpregado;

    private DiaPacote objDiaPacoteSelect;
    private Pacote objPacoteSelect;

    private ArrayList<Empregado> lstEmpregados;
    private ArrayList<Empregado> lstEmpregadosFilter;

    private ArrayList<SelectItem> lstTipo;
    private String titulo;
    private boolean vMensaje = false;
    private boolean vDlgEmpregados = false;

    public DiaPacoteEditController() {
        vMensaje = false;
        estado = SistemaCommon.ActionSistema.novo;
        titulo = "Novo Dia Pacote";
        objDiaPacoteSelect = new DiaPacoteConcrete();
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            objDiaPacoteSelect = (DiaPacoteConcrete) session.getAttribute(SistemaCommon.S_DIA_PACOTE_SELECIONADO);
            objPacoteSelect = (Pacote) session.getAttribute(SistemaCommon.S_PACOTE_SELECIONADO);
            if (objDiaPacoteSelect != null) {
                estado = SistemaCommon.ActionSistema.atualizar;
                titulo = "Editar Dia Pacote";
            }

        } catch (Exception e) {
            LOGGER.error("Error lir usuario no Edit");
        }
    }

    public DiaPacote getObjDiaPacoteSelect() {
        if (objDiaPacoteSelect == null)
            objDiaPacoteSelect = new DiaPacoteConcrete();
        return objDiaPacoteSelect;
    }

    public void setObjDiaPacoteSelect(DiaPacoteConcrete objDiaPacoteSelect) {
        this.objDiaPacoteSelect = objDiaPacoteSelect;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public ArrayList<Empregado> getLstEmpregados() {
        return lstEmpregados;
    }

    public void setLstEmpregados(ArrayList<Empregado> lstEmpregados) {
        this.lstEmpregados = lstEmpregados;
    }

    public boolean isvDlgEmpregados() {
        return vDlgEmpregados;
    }

    public void setvDlgEmpregados(boolean vDlgEmpregados) {
        this.vDlgEmpregados = vDlgEmpregados;
    }

    public ArrayList<Empregado> getLstEmpregadosFilter() {
        return lstEmpregadosFilter;
    }

    public void setLstEmpregadosFilter(ArrayList<Empregado> lstEmpregadosFilter) {
        this.lstEmpregadosFilter = lstEmpregadosFilter;
    }

    public void salvarDados() {
        try {
            ArrayList<String> erros = objDiaPacoteSelect.validar();
            if (erros.size() > 0)
                Util.addErrosMensajens(erros);
            else {
                if (estado == SistemaCommon.ActionSistema.novo) {
                    objDiaPacoteSelect.setPacote(objPacoteSelect);
                    diaPacoteDAO.insert(objDiaPacoteSelect);
                } else
                    diaPacoteDAO.update(objDiaPacoteSelect);
                Util.addInfoMessage("Cadastro Feito");
                vMensaje = true;
            }
        } catch (SQLException e) {
            vMensaje = false;
            Util.addErrorMessage("Erro Cadastro");
            LOGGER.error("Error salavar dados empregado", e);
        }
    }

    public void volver() {
        vMensaje = false;
        Util.redirecionar("/adm/pacote_edit.jsf");
    }

    public void atualizarDiaPacote(DiaPacoteConcrete objDiaPacote) {

    }

    public void mostrarPrestatatarios() {
        try {
            tipoEmpregado = dominioDAO.getByGrupoValor(SistemaCommon.P_TIPOS_EMPREGADO_BD, SistemaCommon.P_VALOR_PRESTATARIO_BD);
            lstEmpregados = empregadoDAO.getAtivosTipoNasDatas(tipoEmpregado.getDominioid(), objPacoteSelect.getDataInicio(), objPacoteSelect.getDataFim());
            //lstEmpregados = empregadoDAO.getByTipoId(tipoEmpregado.getDominioid());
            vDlgEmpregados = true;
        } catch (SQLException e) {
            LOGGER.error("Error listar dominios empregados prestatario", e);
        }
    }

    public void mostrarGuias() {
        try {
            tipoEmpregado = dominioDAO.getByGrupoValor(SistemaCommon.P_TIPOS_EMPREGADO_BD, SistemaCommon.P_VALOR_GUIA_BD);
            lstEmpregados = empregadoDAO.getAtivosTipoNasDatas(tipoEmpregado.getDominioid(), objPacoteSelect.getDataInicio(), objPacoteSelect.getDataFim());
            vDlgEmpregados = true;
        } catch (SQLException e) {
            LOGGER.error("Error listar dominios empregados prestatario", e);
        }
    }

    public void selecionarEmpregado(Empregado objEmpregado) {
        if (tipoEmpregado.getValor().equals(SistemaCommon.P_VALOR_PRESTATARIO_BD))
            objDiaPacoteSelect.setPrestatario(objEmpregado);
        else
            objDiaPacoteSelect.setGuia(objEmpregado);
        vDlgEmpregados = false;
    }

    public void fecharEmpregados() {
        vDlgEmpregados = false;
    }

    public void calculateCustoTotal() {
        try {
            objDiaPacoteSelect.setCustoTotal(objDiaPacoteSelect.getCustobruto());
            DiaPacote objDiaPacoteConcrete = new DiaPacoteConcrete(objDiaPacoteSelect);
            if (objDiaPacoteSelect.getCafe() != null && objDiaPacoteSelect.getCafe().equalsIgnoreCase("S"))
                objDiaPacoteConcrete = new DiaPacoteCafe(objDiaPacoteConcrete);
            if (objDiaPacoteSelect.getAlmorco() != null && objDiaPacoteSelect.getAlmorco().equalsIgnoreCase("S"))
                objDiaPacoteConcrete = new DiaPacoteAlmorco(objDiaPacoteConcrete);
            if (objDiaPacoteSelect.getJantar() != null && objDiaPacoteSelect.getJantar().equalsIgnoreCase("S"))
                objDiaPacoteConcrete = new DiaPacoteJantar(objDiaPacoteConcrete);
            objDiaPacoteSelect.setCustoTotal(objDiaPacoteConcrete.getCustoTotal());
        } catch (Exception e) {
            LOGGER.error("Erro decorator ", e);
        }
    }

}
