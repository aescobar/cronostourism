package br.puc.pss.controller;

import br.puc.pss.agentes.AgenteEnviaAlerta;
import br.puc.pss.agentes.AgenteSugestaoPessoal;
import br.puc.pss.common.SistemaCommon;
import jade.Boot;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/26/13
 * Time: 5:41 AM
 */
@ManagedBean(name = "InitAgentesController", eager = true)
@ApplicationScoped
public class InitAgentesController {
    public InitAgentesController() {
        Boot.main(new String[]{"-gui"});
        inserirAgenteApp(new AgenteSugestaoPessoal());
        inserirAgenteApp(new AgenteEnviaAlerta());
    }

    public void inserirAgenteApp(Agent agente){
        Runtime runtime = Runtime.instance();

        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.CONTAINER_NAME, SistemaCommon.NOME_AGENTES_CONTAINER);

        AgentContainer controllerAgentContainer = runtime.createAgentContainer(profile);

        try {
            AgentController controller;
            controller  = controllerAgentContainer.acceptNewAgent( agente.getClass().getName(), agente);
            controller.start();
        } catch (StaleProxyException ex) {
            System.out.println("Agente nao pode ser iniciado");
        }

    }

}
