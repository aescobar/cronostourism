package br.puc.pss.controller;

import br.puc.pss.DAO.AlertaDAO;
import br.puc.pss.DAO.AlertaDAOImpl;
import br.puc.pss.DAO.UsuarioDAO;
import br.puc.pss.DAO.UsuarioDAOImpl;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Alerta;
import br.puc.pss.obj.Usuario;
import org.apache.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/15/13
 * Time: 3:18 PM
 */
@ManagedBean(name = "LoginController")
@ViewScoped
public class LoginController {
    private UsuarioDAO usuarioDB = new UsuarioDAOImpl();
    private AlertaDAO alertaDAO = new AlertaDAOImpl();

    static final Logger LOGGER = Logger.getLogger(LoginController.class);

    private Usuario objUsuario;
    private String nick;
    private String password;

    public LoginController() {
        this.objUsuario = new Usuario();
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void ingresar() {
        try {

            Usuario usuarioIngreso = usuarioDB.autenticarUsuario(nick, password);
            if (usuarioIngreso != null) {
                LOGGER.info(usuarioIngreso.getNomeusuario());
                objUsuario = usuarioIngreso;
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                session.setAttribute(SistemaCommon.S_USUARIO, usuarioIngreso);
                Util.redirecionar("/adm/usuario.jsf");
            } else
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Los datos son incorrectos", "Los datos son incorrectos"));
        } catch (SQLException e) {
            LOGGER.fatal("Error al consultar la BD ", e);
        }
    }

    public void salir() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.invalidate();
        Util.redirecionar("/login.jsf");
    }

    public Usuario getUsuarioActual() {
        return SistemaCommon.getUsuarioActual();
    }

    private String alertaTit;
    private String style;

    public String getAlertaTit() {

        return alertaTit;
    }

    public void setAlertaTit(String alertaTit) {
        this.alertaTit = alertaTit;
    }

    public String getStyle() {

        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public void getAlertas() {
        ArrayList<Alerta> lstAlertas = new ArrayList<Alerta>();
        try {
            lstAlertas = alertaDAO.getByEstado("AC");
            if (lstAlertas.size() > 0) {
                style = "color: red; font-weight:bold;";
                alertaTit = lstAlertas.size() + " Alertas";
            } else {
                style = "color: black;";
                alertaTit = "0 Alertas";
            }
        } catch (SQLException e) {
            LOGGER.error("Erro lir dados alertas", e);
        }

    }
}
