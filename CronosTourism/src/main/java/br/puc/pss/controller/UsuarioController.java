package br.puc.pss.controller;

import br.puc.pss.DAO.DominioDAO;
import br.puc.pss.DAO.DominioDAOImpl;
import br.puc.pss.DAO.UsuarioDAO;
import br.puc.pss.DAO.UsuarioDAOImpl;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Usuario;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "UsuarioController")
@ViewScoped
public class UsuarioController {
    static final Logger LOGGER = Logger.getLogger(UsuarioController.class);
    private UsuarioDAO usuarioDB = new UsuarioDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();

    private Usuario objUsuarioSelect;
    private ArrayList<Usuario> lstUsuarios;
    private ArrayList<Usuario> lstUsuariosFilter;

    public UsuarioController() {
        if (objUsuarioSelect == null)
            objUsuarioSelect = new Usuario();
    }

    public Usuario getObjUsuarioSelect() {
        return objUsuarioSelect;
    }

    public void setObjUsuarioSelect(Usuario objUsuarioSelect) {
        this.objUsuarioSelect = objUsuarioSelect;
    }

    public ArrayList<Usuario> getLstUsuarios() {
        try {
            lstUsuarios = usuarioDB.getAll();
        } catch (SQLException e) {
            LOGGER.error("Nao se pode recuperar os dados de usuarios", e);
            lstUsuarios = new ArrayList<Usuario>();
        }
        return lstUsuarios;
    }

    public void setLstUsuarios(ArrayList<Usuario> lstUsuarios) {
        this.lstUsuarios = lstUsuarios;
    }

    public ArrayList<Usuario> getLstUsuariosFilter() {
        return lstUsuariosFilter;
    }

    public void setLstUsuariosFilter(ArrayList<Usuario> lstUsuariosFilter) {
        this.lstUsuariosFilter = lstUsuariosFilter;
    }

    public void atulizarUsuario(Usuario objUsuario) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(SistemaCommon.S_USUARIO_SELECIONADO, objUsuario);
            Util.redirecionar("/adm/usuario_edit.jsf");
    }

}
