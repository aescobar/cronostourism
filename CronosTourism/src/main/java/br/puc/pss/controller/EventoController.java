package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Evento;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "EventoController")
@ViewScoped
public class EventoController {
    static final Logger LOGGER = Logger.getLogger(EventoController.class);
    private EventoDAO eventoDAO = new EventoDAOImpl();

    private Evento objEventoSelect;
    private ArrayList<Evento> lstEventos;
    private ArrayList<Evento> lstEventosFilter;

    public EventoController() {
        if (objEventoSelect == null)
            objEventoSelect = new Evento();
    }

    public Evento getObjEmpregadoSelect() {
        return objEventoSelect;
    }

    public void setObjEventoSelect(Evento objEventoSelect) {
        this.objEventoSelect = objEventoSelect;
    }

    public ArrayList<Evento> getLstEventos() {
        try {
            lstEventos = eventoDAO.getAll();
        } catch (SQLException e) {
            LOGGER.error("Nao se pode recuperar os dados de eventos", e);
            lstEventos = new ArrayList<Evento>();
        }
        return lstEventos;
    }

    public void setLstEventos(ArrayList<Evento> lstEventos) {
        this.lstEventos = lstEventos;
    }

    public ArrayList<Evento> getLstEventosFilter() {
        return lstEventosFilter;
    }

    public void setLstEventosFilter(ArrayList<Evento> lstEventosFilter) {
        this.lstEventosFilter = lstEventosFilter;
    }

    public void atulizarEvento(Evento objEvento) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute(SistemaCommon.S_EVENTO_SELECIONADO, objEvento);
        Util.redirecionar("/adm/evento_edit.jsf");
    }

    public String dateToStringShort(Date fecha) {
        return Util.dateToStringShortWebApp(fecha);
    }

}
