package br.puc.pss.controller;

import br.puc.pss.DAO.DominioDAO;
import br.puc.pss.DAO.DominioDAOImpl;
import br.puc.pss.DAO.PacoteDAO;
import br.puc.pss.DAO.PacoteDAOImpl;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Pacote;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "PacoteController")
@ViewScoped
public class PacoteController {
    static final Logger LOGGER = Logger.getLogger(PacoteController.class);
    private PacoteDAO pacoteDAO = new PacoteDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();

    private Pacote objPacoteSelect;
    private ArrayList<Pacote> lstPacotes;
    private ArrayList<Pacote> lstPacotesFilter;

    public PacoteController() {
        if (objPacoteSelect == null)
            objPacoteSelect = new Pacote();
    }

    public Pacote getObjPacoteSelect() {
        return objPacoteSelect;
    }

    public void setObjPacoteSelect(Pacote objPacoteSelect) {
        this.objPacoteSelect = objPacoteSelect;
    }

    public ArrayList<Pacote> getLstPacotes() {
        try {
            lstPacotes = pacoteDAO.getAllByDateIncioAndFim(new Date(), null);
        } catch (SQLException e) {
            LOGGER.error("Nao se pode recuperar os dados de empregados", e);
            lstPacotes= new ArrayList<Pacote>();
        }
        return lstPacotes;
    }

    public void setLstPacotes(ArrayList<Pacote> lstEmpregados) {
        this.lstPacotes = lstEmpregados;
    }

    public ArrayList<Pacote> getLstPacotesFilter() {
        return lstPacotesFilter;
    }

    public void setLstPacotesFilter(ArrayList<Pacote> lstPacotesFilter) {
        this.lstPacotesFilter = lstPacotesFilter;
    }

    public void atulizarPacote(Pacote objPacote){
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(SistemaCommon.S_PACOTE_SELECIONADO, objPacote);
            Util.redirecionar("/adm/pacote_edit.jsf");
    }

    public String dateToStringShort(Date fecha){
        return Util.dateToStringShortWebApp(fecha);
    }

    public String formatDescricao(String dado){
        if(dado.length() > 30)
            dado = dado.substring(0,30)+" ...";
        return dado;
    }

}
