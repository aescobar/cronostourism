package br.puc.pss.controller;

import br.puc.pss.DAO.AlertaDAO;
import br.puc.pss.DAO.AlertaDAOImpl;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Alerta;
import br.puc.pss.obj.Evento;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "AlertaController")
@ViewScoped
public class AlertaController {
    static final Logger LOGGER = Logger.getLogger(AlertaController.class);
    private AlertaDAO alertaDAO = new AlertaDAOImpl();

    private Evento objEventoSelect;
    private ArrayList<Alerta> lstAlertas;
    private ArrayList<Alerta> lstAlertasFilter;

    public AlertaController() {
        if (objEventoSelect == null)
            objEventoSelect = new Evento();
    }

    public Evento getObjEmpregadoSelect() {
        return objEventoSelect;
    }

    public void setObjEventoSelect(Evento objEventoSelect) {
        this.objEventoSelect = objEventoSelect;
    }

    public ArrayList<Alerta> getLstAlertas() {
        try {
            lstAlertas = alertaDAO.getByEstado("AC");
        } catch (SQLException e) {
            LOGGER.error("Nao se pode recuperar os dados de eventos", e);
            lstAlertas = new ArrayList<Alerta>();
        }
        return lstAlertas;
    }

    public void setLstAlertas(ArrayList<Alerta> lstAlertas) {
        this.lstAlertas = lstAlertas;
    }

    public ArrayList<Alerta> getLstAlertasFilter() {
        return lstAlertasFilter;
    }

    public void setLstAlertasFilter(ArrayList<Alerta> lstAlertasFilter) {
        this.lstAlertasFilter = lstAlertasFilter;
    }

    public void atulizarAlerta(Alerta objAlerta) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute(SistemaCommon.S_EVENTO_SELECIONADO, objAlerta);
        Util.redirecionar("/adm/evento_edit.jsf");
    }

    public String dateToStringShort(Date fecha) {
        return Util.dateToStringShortWebApp(fecha);
    }

}
