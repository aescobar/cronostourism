package br.puc.pss.controller;

import br.puc.pss.DAO.*;
import br.puc.pss.common.SistemaCommon;
import br.puc.pss.common.Util;
import br.puc.pss.obj.Dominio;
import br.puc.pss.obj.Empregado;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio
 * User: Ariel Escobar
 * Date: 11/16/13
 * Time: 8:28 PM
 */
@ManagedBean(name = "EmpregadoEditController")
@ViewScoped
public class EmpregadoEditController {

    static final Logger LOGGER = Logger.getLogger(EmpregadoEditController.class);

    private EmpregadoDAO empregadoDAO = new EmpregadoDAOImpl();
    private DominioDAO dominioDAO = new DominioDAOImpl();

    private SistemaCommon.ActionSistema estado = SistemaCommon.ActionSistema.novo;

    private Empregado objEmpregadoSelect;

    private ArrayList<SelectItem> lstTipo;
    private String objTipoSelect;
    private String titulo;
    private boolean vMensaje = false;

    public EmpregadoEditController() {
        vMensaje = false;
        estado = SistemaCommon.ActionSistema.novo;
        titulo = "Novo Empregado";
        objEmpregadoSelect = new Empregado();
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            objEmpregadoSelect = (Empregado) session.getAttribute(SistemaCommon.S_EMPREGADO_SELECIONADO);
            if (objEmpregadoSelect != null){
                estado = SistemaCommon.ActionSistema.atualizar;
                titulo = "Editar Empregado";
            }
        } catch (Exception e) {
            LOGGER.error("Error lir usuario no Edit");
        }
    }

    public Empregado getObjEmpregadoSelect() {
        if (objEmpregadoSelect == null)
            objEmpregadoSelect = new Empregado();
        return objEmpregadoSelect;
    }

    public void setObjEmpregadoSelect(Empregado objEmpregadoSelect) {
        this.objEmpregadoSelect = objEmpregadoSelect;
    }

    public ArrayList<SelectItem> getLstTipo() {
        if (lstTipo == null || lstTipo.size() < 1) {
            try {
                lstTipo = new ArrayList<SelectItem>();
                ArrayList<Dominio> lstPaisesDom = dominioDAO.getByGrupo(SistemaCommon.P_TIPOS_EMPREGADO_BD);
                for (Dominio itemDominio : lstPaisesDom) {
                    SelectItem i = new SelectItem(itemDominio.getDominioid(), itemDominio.getDescricao());
                    lstTipo.add(i);
                }
            } catch (SQLException e) {
                lstTipo = new ArrayList<SelectItem>();
            }
        }
        return lstTipo;
    }

    public void setLstTipo(ArrayList<SelectItem> lstTipo) {
        this.lstTipo = lstTipo;
    }

    public String getObjTipoSelect() {
        Long x = objEmpregadoSelect.getTipo().getDominioid();
        if (x != null)
            objTipoSelect = x + "";
        return objTipoSelect;
    }

    public void setObjTipoSelect(String objTipoSelect) {
        this.objTipoSelect = objTipoSelect;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public void salvarDados() {
        try {
            ArrayList<String> erros = objEmpregadoSelect.validar();
            if (erros.size() > 0)
                Util.addErrosMensajens(erros);
            else {
                if (estado == SistemaCommon.ActionSistema.novo)
                    empregadoDAO.insert(objEmpregadoSelect);
                else
                    empregadoDAO.update(objEmpregadoSelect);
                Util.addInfoMessage("Cadastro Feito");
                vMensaje = true;
            }
        } catch (SQLException e) {
            vMensaje = false;
            Util.addErrorMessage("Erro Cadastro");
            LOGGER.error("Error salavar dados empregado", e);
        }
    }

    public void volver() {
        vMensaje = false;
        Util.redirecionar("/adm/empregado.jsf");
    }


}
