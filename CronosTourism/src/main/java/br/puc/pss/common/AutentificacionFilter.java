package br.puc.pss.common;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar
 * Pontificia Universidade Catolica de Rio de Janeiro
 * Date: 7/10/13
 * Time: 4:36 PM
 */
public class AutentificacionFilter implements Filter {

    private FilterConfig configuration;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.configuration = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (((HttpServletRequest) request).getSession().getAttribute(SistemaCommon.S_USUARIO) == null) {
            ((HttpServletResponse) response).sendRedirect("../login.jsf");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        configuration = null;
    }
}
