package br.puc.pss.common;


import br.puc.pss.obj.Usuario;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:35 AM
 */
public class SistemaCommon {
    public final static String S_USUARIO = "USUARIO";
    public final static String S_USUARIO_SELECIONADO = "USUARIO_SELECIONADO";
    public final static String S_EMPREGADO_SELECIONADO = "EMPREGADO_SELECIONADO";
    public final static String S_PACOTE_SELECIONADO = "PACOTE_SELECIONADO";
    public final static String S_DIA_PACOTE_SELECIONADO = "DIA_PACOTE_SELECIONADO";
    public final static String S_EVENTO_SELECIONADO = "EVENTO_SELECIONADO";

    public enum ActionSistema {
        novo,
        atualizar,
        deletar,
    }

    public final static BigDecimal custoCafe = new BigDecimal(10);
    public final static BigDecimal custoAlmorco = new BigDecimal(20);
    public final static BigDecimal custoJantar = new BigDecimal(20);

    public final static String NOME_AGENTES_CONTAINER = "AgentesConstainer";


    public final static String P_PADRAO_VALID_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public final static String P_TIPOS_USUARIO_BD = "TIPOS_USUARIO";
    public final static String P_TIPOS_EMPREGADO_BD = "TIPOS_EMPREGADO";
    public final static String P_VALOR_PRESTATARIO_BD = "P";
    public final static String P_VALOR_GUIA_BD = "G";
    public final static String P_TIPOS_PACOTE_BD = "TIPOS_PACOTE";


    public final static String SMS_URL = "http://217.118.27.5/bulksms/bulksend.go";
    public final static String SMS_USER = "ralcocer@mc4.com.bo";
    public final static String SMS_PASSWORD = "058F2257";
    public final static String SMS_PROVIDER = "Fortaleza";

    public static Usuario getUsuarioActual() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            return (Usuario) session.getAttribute(SistemaCommon.S_USUARIO);
        } catch (Exception e) {
            return new Usuario();
        }
    }

}
